# About

Tag Extractor is a command line script allowing to interact with Urban Airship API in order to extract the list of tags that are set on all channels of a given UA Application.

The script relies on a [fork of the urbanairship gem](https://github.com/FTBpro/urbanairship/commits/response_next_page_and_channels_api) that provides access to the UA channels and links to the next page of results whenever this is available.

# Installation

## Clone Repository

    mkdir tag_extractor
    cd tag_extractor
    git clone git@bitbucket.org:jbonney/urban-airship-tag-extractor.git .

##  Install Gems

The repository comes with RVM configuration files. It requires Ruby 2.1.2 (but this can easily be changed in `.ruby-version`). The `ua_tag_extractor` gemset will be created the first time you enter the project directory (this can also be changed in `.ruby-gemset`).

    cd ..
    cd tag_extractor
    bundle install

## Create Configuration File

The script relies on the Urban Airship API keys. They need to be configured in `config/credentials.json`. To create the file, simply run;

    ./bin/ua_cli install [--force]

The following parameter is accepted:

* `--force`: re-create `config/credentials.json` and overwrite existing values.

# Run

There is currently only one operation possible:

    ./bin/ua_cli tags [--active] [--group group_name] [--counter] [--output path/to/tags.csv] [--verbose]

The following parameters are accepted:

* `--active`: if no output file is provided, only the tags from active / installed channels will be displayed on screen
* `--group group_name`: if provided, the script will extract tags from the given tag group (`tag_groups` defined in UA), otherwise it will simply extract the device tags
* `--counter`: display the number of tags on screen
* `--output path/to/tags.csv`: create a CSV file containing the list of tags as well as information about the number of occurrences of each tag (split between active and inactive devices)
* `--verbose`: activate verbose output while running the script

# Requirements

The script requires Ruby to be installed on your system (see section Install Gems). Ideally, this should be installed through RVM (or equivalent).

# Console

In order to test some of the functions in an interactive console, once can use `pry` as illustrated below.

Launch a console:

    pry

Load necessary dependencies:

    require_relative 'lib/tag_extractor'

Initiate:

    ua = TagExtractor::UrbanAirshipManager.new
    ua.get_tags

# Notes

* The tags extracted by the script are taken directly from the channels. This means that if some tags have been created but no device yet have been tagged with them, they will not appear in the list.
