module TagExtractor
  class UrbanAirshipManager
    # Configuration file variable
    CONFIG_FILE = "config/credentials.json"
    CONFIG_FILE_PATH = File.join(File.dirname(__FILE__), "/../../#{CONFIG_FILE}")

    # UA client
    attr_accessor :client
    # List of tags
    attr_accessor :tags_active
    attr_accessor :tags_inactive
    attr_accessor :tags_details
    # Verbosity of operations
    attr_accessor :verbose

    #
    # Class method that ensures that the configuration file is created properly
    # @param  force [boolean] overwrite existing file if it exists already
    #
    # @return [void] the configuration file is created
    def self.initiate_config_file(force)
      # Create configuration file
      create_config_file(force)
    end

    def initialize(params)
      # Ensure that the configuration file is properly set
      validate_config_file
      # Initialize Urban Airship client
      set_ua_client
      # Initialize a new empty set
      self.tags_active = Set.new
      self.tags_inactive = Set.new
      self.tags_details = Hash.new
      # Assign verbosity
      self.verbose = params[:verbose] if params[:verbose]
    end

    #
    # Get list of tags from the channels with info about the number
    # of channels (active and inactive) on which the tag can be found
    # @param  tag_group = nil [String] only extract tags from a specific group, if no group is specified, then device tags are considered
    #
    # @return [Hash] the list of tags with the following format:
    # {
    #   :"My First Tag"=>{
    #     :name=>"My First Tag", :active_count=>1, :inactive_count=>3
    #   },
    #   :"My Second Tag"=>{
    #     :name=>"My Second Tag", :active_count=>6, :inactive_count=>3
    #   }
    # }
    def get_tags_details(tag_group = nil)
      puts "Get tag details" if self.verbose

      fetch_tags_from_channels(tag_group)
      self.tags_details.sort
    end

    #
    # Get list of tags from the channels
    # @param  only_active = false [Boolean] whether to consider only active channels
    # @param  tag_group = nil [String] only extract tags from a specific group, if no group is specified, then device tags are considered
    #
    # @return [Set] the list of tags
    def get_tags(only_active = false, tag_group = nil)
      puts "Get tags for active devices" if self.verbose && only_active
      puts "Get tags for all devices" if self.verbose && !only_active

      fetch_tags_from_channels(tag_group)
      tags = Set.new
      if only_active
        tags = self.tags_active.sort
      else
        tags = (self.tags_active + self.tags_inactive).sort
      end
      tags
    end

    #
    # Perform operations on channels fetched from Urban Airship.
    # Operations are performed in batch (i.e. on each batch of channels
    # fetched from UA.)
    # @param channel_analysis [Proc] the operation to perform on each channel
    #
    def process_channels(channel_analysis)
      puts "Go through UA channels" if self.verbose
      # Get the first page of tokens (usually 1000 items)
      channels_page = nil
      # Try fetching the page again and again in case of error
      # Timeout is quite a common issue and we therefore try multiple times to fetch the page
      loop do
        puts "--> trying to fetch list of channels (page 1)" if self.verbose
        channels_page = self.client.channels
        # Break the loop when there are no errors
        break if !channels_page.has_key?("error")
      end
      puts "--> fetching page 1 of devices" if self.verbose
      # Initializing counter (used for logging purposes)
      local_counter = 2
      total_counter = 0
      # Loop to go through the pages of results
      loop do
        begin
          # Get the number of channels in the result page
          current_batch_size = channels_page["channels"].size
          puts "----> going through #{current_batch_size} channels (already processed #{total_counter} channels)" if self.verbose
          # Fetch channels from the answer
          channels = channels_page["channels"]
          # Go through the channels and perform the provided operation
          channels.each do |channel|
            channel_analysis.call(channel)
          end
          puts "--> fetching page #{local_counter} of devices #{channels_page["next_page"].nil? ? " - final page" : ""}" if self.verbose
          # Update number of pages already processed
          local_counter += 1
          # Update total number of channels analyzed
          total_counter += current_batch_size
          # Fetch next page of results
          current_channels_page = nil
          next_channels_page = nil
          loop do
            puts "--> trying to fetch next page of channels (page #{local_counter})" if self.verbose
            # Use temporary variable in case we need to retry multiple times
            current_channels_page = channels_page
            next_channels_page = current_channels_page.next_page
            # Break the loop if there wasn't any next page
            break if next_channels_page.nil?
            # Break the loop when there are no errors
            break if !next_channels_page.has_key?("error")
          end
          channels_page = next_channels_page
          # Quit the loop if there is nothing more to process, i.e. if the channels_page is nil
          break unless channels_page
        rescue StandardError => e
          STDERR.puts "Error while requesting channels page: #{channels_page["error"]}" if channels_page["error"]
          STDERR.puts "Issue while fetching channel tags: #{e.inspect}"
          exit 1
        end
      end
    end

    private

    #
    # Class method that creates the configuration file. If the force parameter is passed,
    # then any existing file will be overwritten.
    # @param  force = false [Boolean] true to overwrite existing configuration file
    #
    # @return [void] file is created on success and information message is displayed on standard output.
    def self.create_config_file(force = false)
      # Check if the file already exist. If this is the case and user has not specified the --force
      # option, then display an error message to prevent unwanted overwrite.
      if File.exists?(CONFIG_FILE_PATH)
        unless force
          STDERR.puts "A configuration file already exists. If you want to re-create it (and overwrite any existing value), then specify the `force` parameter."
          exit 1
        end
      end
      # If we arrive here, it is either because the file doesn't exist already OR because
      # it exists but the force parameter has been specified.
      # Define empty credentials options
      credentials = {
        :application_key => "",
        :application_secret => "",
        :master_secret => ""
      }
      # Open the file and write the configuration values
      File.open(CONFIG_FILE_PATH, 'w') {|file| file.write(JSON.pretty_generate(credentials))}
      # Exit the program and display an informative message to the user
      puts "Configure the application key, application secret and master secret in credentials.json"
      exit 0
    end

    #
    # Verify if configuration file is available
    #
    # @return [void] exit the program if the configuration file is not available
    def validate_config_file(force = false)
      # Check if the configuration file exists
      unless File.exists? CONFIG_FILE_PATH
        # Exit the program and display an informative message to the user
        STDERR.puts "No configuration file found. Have you tried to run: 'ua_cli install'?"
        exit 1
      end
    end

    #
    # Set UA client and confirm that the credentials are correct.
    # To confirm that the client can connect, we make a call to
    # `tags` and ensure that we do not get an error back.
    #
    # @return [void] the UA client is set and programs exists if the
    # client cannot connect.
    def set_ua_client
      # Initialize Urban Airship client
      self.client = Urbanairship::Client.new
      # Load UrbanAirship credentials from file
      creds = JSON.load(File.read(CONFIG_FILE))
      self.client.application_key = creds["application_key"]
      self.client.application_secret = creds["application_secret"]
      self.client.master_secret = creds["master_secret"]
      # Increasing default timeout as 5 seconds is relatively short and introduces lots of errors
      self.client.request_timeout = 20
      puts "Initialized Urban Airship client" if self.verbose
      # Check if the client can connect
      unless self.client.tags.success?
        STDERR.puts "Client couldn't connect to UrbanAirship. Please check that the credentials provided are correct."
        exit 1
      end
    end

    #
    # Generic method to fetch tags from channels.
    # Based on the presence of a tag group, the method will dispatch
    # the call either to fetch the device tags or the tags of a given group.
    # @param  tag_group = nil [String] only extract tags from a specific group, if no group is specified, then device tags are considered
    #
    # @return [void] dispatch the call
    def fetch_tags_from_channels(tag_group = nil)
      if tag_group
        puts "Fetch tags from group #{tag_group}" if self.verbose
        fetch_group_tags_from_channels(tag_group)
      else
        puts "Fetch device tags" if self.verbose
        fetch_device_tags_from_channels
      end
    end

    #
    # Get device tags from all channels
    #
    # @return [Set] the list of tags from channels
    def fetch_device_tags_from_channels
      tag_operation = Proc.new {|channel|
        if channel["installed"] == true
          channel["tags"].each do |tag|
            self.tags_active << tag
            add_tag_to_collection(tag, true)
          end
        else
          channel["tags"].each do |tag|
            self.tags_inactive << tag
            add_tag_to_collection(tag, false)
          end
        end
      }
      process_channels(tag_operation)
    end

    #
    # Get tags from a specific group from all channels
    #
    # @return [Set] the list of tags from channels
    def fetch_group_tags_from_channels(tag_group)
      tag_operation = Proc.new {|channel|
        if channel["tag_groups"] && channel["tag_groups"][tag_group]
          if channel["installed"] == true
            channel["tag_groups"][tag_group].each do |tag|
              self.tags_active << tag
              add_tag_to_collection(tag, true)
            end
          else
            channel["tag_groups"][tag_group].each do |tag|
              self.tags_inactive << tag
              add_tag_to_collection(tag, false)
            end
          end
        end
      }
      process_channels(tag_operation)
    end

    #
    # Add tag to the tag details hash
    # @param  name [String] the tag name
    # @param  active [Boolean] whether the tag is coming from an active channel
    #
    # @return [void] the tag details are saved in the tag_details object
    def add_tag_to_collection(name, active)
      # Check if the tag is already in the list
      if self.tags_details.has_key?(name.to_sym)
        # If it is coming from an active channel...
        if active
          # ... then increase the active count
          self.tags_details[name.to_sym][:active_count] += 1
        else
          # ... then increase the inactive count
          self.tags_details[name.to_sym][:inactive_count] += 1
        end
      else
        # If the tag is not yet in the list, then add a new entry
        # and initiate the active / inactive counters
        self.tags_details[name.to_sym] = {
          :name => name,
          :active_count => active ? 1 : 0,
          :inactive_count => active ? 0 : 1
        }
      end
    end

  end
end
