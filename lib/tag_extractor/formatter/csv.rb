module TagExtractor
  module Formatter
    class CSV
      attr_accessor :file

      def initialize(path)
        self.file = path
      end

      def write(tags)
        # Start by writing the header line
        File.open(self.file, 'w') do |file|
          file.printf("%s,%s,%s\n", "Tag", "Active devices", "Inactive devices")
          tags.each do |tag, details|
            file.printf("%s,%d,%d\n", details[:name], details[:active_count], details[:inactive_count])
          end
        end
      end
    end
  end
end
