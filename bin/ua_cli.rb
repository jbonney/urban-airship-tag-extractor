#!/usr/bin/env ruby

# Add the lib folder to the path
$LOAD_PATH << File.expand_path(File.dirname(__FILE__) + '/../lib')

# Require bundler to load gems from Git source (urbanairship gem taken from git)
# http://stackoverflow.com/questions/2577346/how-to-install-gem-from-github-source#comment15256259_7421712
require 'bundler'
Bundler.setup(:default)

# Only require thor (for CLI handling)
require 'thor'
# All other gems are required together in lib/tag_extractor.rb
require 'tag_extractor'

class UACLI < Thor
  desc "install", "Create configuration file"
  option :force, :type => :boolean, :desc => "Overwrite existing file with default config file"
  def install
    TagExtractor::UrbanAirshipManager.initiate_config_file(options[:force])
  end

  desc "tags", "Get all tags associated to existing devices"
  option :active, :type => :boolean, :desc => "Only list active devices"
  option :counter, :type => :boolean, :desc => "Display the number of unique tags on the console"
  option :group, :desc => "Extract tags from a specific tag group"
  option :output, :desc => "Export list of tags in a CSV file"
  option :verbose, :type => :boolean, :desc => "Display verbose information"
  def tags
    # Initiate Urban Airship
    ua_manager = TagExtractor::UrbanAirshipManager.new({:verbose => options[:verbose]})
    # Fetch the tags
    if options[:output]
      tags = ua_manager.get_tags_details(options[:group])
      formatter = TagExtractor::Formatter::CSV.new(options[:output])
      formatter.write(tags)
    else
      tags = ua_manager.get_tags(options[:active], options[:group])
      p tags
    end
    # Display the number of unique tags on screen
    if options[:counter]
      p tags.size
    end
  end
end

UACLI.start(ARGV)
